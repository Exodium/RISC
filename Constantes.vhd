library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package formats is
	constant ADR : integer := 4;
	constant REG_SIZE : integer := 8;
	constant SHORT : integer := 16;
	constant FULL_COMMAND_SIZE : integer := 24;
	constant COMMAND_SIZE : integer := 4;
	constant ALU_OPCODE_SIZE : integer := 4;

	constant COMMAND_START : integer := FULL_COMMAND_SIZE-1;
	constant COMMAND_END : integer := FULL_COMMAND_SIZE-COMMAND_SIZE;
end package formats;

package alu_opcodes is
	constant ALU_NOP : integer := 0; -- cond = false
	constant ALU_ADD : integer := 1; -- cond = overflow
	constant ALU_SUB : integer := 2; -- cond = overflow
	constant ALU_AND : integer := 3; -- cond = (A==0)
	constant ALU_OR  : integer := 4; -- cond = (A!=0)
	constant ALU_XOR : integer := 5;
	constant ALU_NOR : integer := 6;
	constant ALU_MUL : integer := 7;
	constant ALU_SLL : integer := 8;
	constant ALU_SRL : integer := 9;
end package alu_opcodes;

package commands is
	constant CMD_ADD   : integer := 0; -- et NOP et MOV et SET et INCR
	constant CMD_SUB   : integer := 1; -- et NEG et DECR
	constant CMD_AND   : integer := 2; -- et if0
	constant CMD_OR    : integer := 3;
	constant CMD_XOR   : integer := 4;
	constant CMD_NOR   : integer := 5;
	constant CMD_MUL   : integer := 6;
	constant CMD_SLL   : integer := 7;
	constant CMD_SRL   : integer := 8;
	constant CMD_JMPc  : integer := 9;
	constant CMD_LOAD  : integer := 10;
	constant CMD_STORE : integer := 11;
end package commands;