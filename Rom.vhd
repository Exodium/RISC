-- ROM : 256*32 bits

library ieee;

use IEEE.STD_LOGIC_1164.ALL;
Use ieee.numeric_std.all ;

use work.formats.all;

entity rom is
	port(
			en			:	in std_logic;
			clk		:	in std_logic;
			rst		:	in std_logic;
			Adress	:	in std_logic_vector(REG_SIZE-1 downto 0);
			Data_out:	out std_logic_vector(FULL_COMMAND_SIZE-1 downto 0)
			);
end rom;

architecture rom_a of rom is

type rom is array(0 to 255) of std_logic_vector(FULL_COMMAND_SIZE-1 downto 0);

signal Data_Rom : rom ;



--------------- BEGIN -----------------------------------------------------------------
begin
-- rw='1' alors lecture
	acces_rom:process(rst, clk)
		begin
		
		if rst='1' then
			Data_Rom(0) <= "000011100000001000000010";
			Data_Rom(1) <= "000011110000000100000000";
			Data_Rom(2) <= "001000010000000000000000";
			Data_Rom(3) <= "100111101110000000000000";
			Data_Rom(4) <= "010000010010011100000000";
			Data_Rom(5) <= "000000010000010000000000";
			Data_Rom(6) <= "000000101110001000000001";
			Data_Rom(7) <= "000000000000000000000000";
			Data_Rom(8) <= "000000000000000000000000";
			Data_Rom(9) <= "000000000000000000000000";
			Data_Rom(10) <= "001001110000000000000000";
			Data_Rom(11) <= "100111101110000000001100";
			Data_Rom(12) <= "001001001110001110000000";
			Data_Rom(13) <= "000101000010010000000000";
			Data_Rom(14) <= "000000000000000000000000";
			Data_Rom(15) <= "000000000000000000000000";
			Data_Rom(16) <= "000000000000000000000000";
			Data_Rom(17) <= "001001000000000000000000";
			Data_Rom(18) <= "100111101110000000010111";
			Data_Rom(19) <= "001000110000000000000000";
			Data_Rom(20) <= "100111101110000000001100";
			Data_Rom(21) <= "001000000000000000000000";
			Data_Rom(22) <= "100111101110000000000100";
			Data_Rom(23) <= "000000100000111100000000";
			Data_Rom(24) <= "001000000000000000000000";
			Data_Rom(25) <= "100111101110000000000000";

			Data_out <= (others=>'0');
		else if rising_edge(clk) then
				if en='1'then
					Data_out <= Data_Rom(to_integer(unsigned(Adress)));
				end if;
			end if;
		end if;
		
	end process acces_rom;

end rom_a;
