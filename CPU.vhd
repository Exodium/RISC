-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- PROGRAM		"Quartus Prime"
-- VERSION		"Version 18.0.0 Build 614 04/24/2018 SJ Lite Edition"
-- CREATED		"Tue Jan 12 09:49:06 2021"

library ieee;
use ieee.std_logic_1164.ALL; 
use ieee.numeric_std.ALL; 


library work;
use work.formats.ALL;

ENTITY CPU IS 
	PORT
	(
		MAX10_CLK1_50 :  IN  STD_LOGIC;
		RST : IN STD_LOGIC;

		SW :  IN  STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		HEX1 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		HEX2 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		HEX3 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		HEX4 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		HEX5 :  OUT  STD_LOGIC_VECTOR(7 DOWNTO 0);
		LEDR :  OUT  STD_LOGIC_VECTOR(9 DOWNTO 0)
		
	);
END CPU;

ARCHITECTURE bdf_type OF CPU IS 



COMPONENT seg7_lut
	PORT(iDIG : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 oSEG : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
	);
END COMPONENT;

COMPONENT dig2dec
	PORT(vol : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 seg0 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 seg1 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 seg2 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 seg3 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		 seg4 : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	);
END COMPONENT;


SIGNAL	zero :  STD_LOGIC;
SIGNAL	one :  STD_LOGIC;
SIGNAL	HEX_out0 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	HEX_out1 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	HEX_out2 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	HEX_out3 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	HEX_out4 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	seg7_in0 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	seg7_in1 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	seg7_in2 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	seg7_in3 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	seg7_in4 :  STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	seg7_in5 :  STD_LOGIC_VECTOR(7 DOWNTO 0);


component command is
	Port ( 
		command_input : in STD_LOGIC_VECTOR(FULL_COMMAND_SIZE-1 downto 0);
		
		comp : in std_logic;

		alu_opcode : out STD_LOGIC_VECTOR(ALU_OPCODE_SIZE-1 downto 0);
		
		-- arguments pour la file de registres
		cmd_addr_A : out STD_LOGIC_VECTOR(ADR-1 downto 0);
		cmd_addr_B : out STD_LOGIC_VECTOR(ADR-1 downto 0);
		
		-- où stocker les résultats de l'ALU
		cmd_addr_C : out STD_LOGIC_VECTOR(ADR-1 downto 0);
		
		command_imm_value : out std_logic_vector(7 downto 0);

		jump : out std_logic;
		--out_jump : out std_logic;

		out_ram_enable : out std_logic;
		out_ram_rw : out std_logic;
		
		-- clock
		clk : in std_logic;
		-- reset
		rst : in std_logic
		);
end component;



component reg_table is
	port (
		clk : in std_logic;
		rst : in std_logic;

		in_imm : in STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);

		in_adrA : in STD_LOGIC_VECTOR(ADR-1 downto 0);
		out_valA : out STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);

		in_adrB : in STD_LOGIC_VECTOR(ADR-1 downto 0);
		out_valB : out STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);


		in_adrC : in STD_LOGIC_VECTOR(ADR-1 downto 0);
		in_valC : in STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);


		in_sys_in : in STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);
		out_sys_out : out STD_LOGIC_VECTOR(REG_SIZE-1 downto 0));
end component;

component ALU is 
	port ( 
		clk : in std_logic;
		rst : in std_logic;
		A, B : in std_logic_vector (REG_SIZE-1 downto 0);

		alu_opcode : in std_logic_vector (ALU_OPCODE_SIZE-1 downto 0);
		C  : out std_logic_vector (REG_SIZE-1 downto 0);
		cond_out : out std_logic
	);
end component;

component Fetch is 
	port(
		en			:	in std_logic;
		clk		:	in std_logic;
		rst		:	in std_logic;
		PC_load	:	in std_logic;
		PC_jump	:	in std_logic_vector(7 downto 0);
		PC_out	:	out std_logic_vector(7 downto 0)
	);
end component;

component rom is
	port(
			en			:	in std_logic;
			clk		:	in std_logic;
			rst		:	in std_logic;
			Adress	:	in std_logic_vector(REG_SIZE-1 downto 0);
			Data_out:	out std_logic_vector(FULL_COMMAND_SIZE-1 downto 0)
			);
end component;

component ram is
	port(
		rw,en		:	in std_logic;
		clk		:	in std_logic;
		rst		:	in std_logic;
		Adress	:	in std_logic_vector(7 downto 0);
		Data_in	:	in std_logic_vector(7 downto 0);
		Data_out:	out std_logic_vector(7 downto 0)
		);
end component;

signal rom_read : STD_LOGIC_VECTOR(FULL_COMMAND_SIZE-1 downto 0);
signal ram_out : STD_LOGIC_VECTOR(7 downto 0);
signal alu_opcode : STD_LOGIC_VECTOR(ALU_OPCODE_SIZE-1 downto 0);
signal adrA,adrB,adrC : STD_LOGIC_VECTOR(3 downto 0);
signal A,B, C : std_logic_vector(7 downto 0);
signal imm : std_logic_vector(7 downto 0);


signal PC_out : std_logic_vector(7 downto 0);
signal ctr_in : std_logic_vector(7 downto 0);
signal cond, jmp : std_logic;

signal rw, en : std_logic;
signal sys_out : std_logic_vector(REG_SIZE-1 downto 0);

signal A_d, B_d, C_d : std_logic_vector(REG_SIZE-1 downto 0);

BEGIN 

	FetchUnit : Fetch 
		port map (
			en => '1',
			clk => MAX10_CLK1_50,
			rst => RST,
			PC_load => jmp,
			PC_jump => C,
			PC_out => PC_out
		);

	Rom1 : rom 
		port map (
			en => '1',
			clk => MAX10_CLK1_50,
			rst => RST,
			Adress => PC_out,
			Data_out => rom_read
		);

	Decodeur1 : command
		port map ( 
			-- entrees
			rst => RST ,
			clk => MAX10_CLK1_50,
			command_input => rom_read,
			
			-- sorties
			alu_opcode => alu_opcode, -- opération de l'ALU (va changer de taille ?)
			cmd_addr_A => adrA, -- arguments pour l'ALU
			cmd_addr_B => adrB,
			cmd_addr_C => adrC, -- où stocker les résultats de l'ALU
			command_imm_value => imm,
			
			comp => Cond,
			jump => jmp,
			
			out_ram_enable => en,
			out_ram_rw => rw
			);

	RegTable1 : reg_table
		port map (
			-- entrées
			rst => rst,
			clk => MAX10_CLK1_50,

			in_adrA => adrA,
			in_adrB => adrB,

			in_adrC => adrC,
			in_valC => C_d,

			in_imm => imm,

			in_sys_in => SW(REG_SIZE-1 downto 0),
			-- sorties
			out_valA => A,
			out_valB => B,

			out_sys_out => sys_out

		);

	ALU1 : ALU
		port map (
			-- entrées
			rst => RST ,
			Clk => MAX10_CLK1_50,
			A => A,
			B => B,
			
			alu_opcode => alu_opcode,

			-- sorties
			C => C,
			cond_out => Cond 
		);



	ram1 : ram
		port map (
			clk => MAX10_CLK1_50,
			rst => RST, 
			Adress => A_d,
			Data_in => B_d,
			rw => rw,
			en => en,
			Data_out => ram_out
		);





--- ça on s'en fout


	b2v_inst : seg7_lut
	PORT MAP(iDIG => seg7_in0,
			oSEG => HEX_out4(6 DOWNTO 0));


	b2v_inst1 : seg7_lut
	PORT MAP(iDIG => seg7_in1,
			oSEG => HEX_out3(6 DOWNTO 0));

	b2v_inst2 : seg7_lut
	PORT MAP(iDIG => seg7_in2,
			oSEG => HEX_out2(6 DOWNTO 0));

	b2v_inst3 : seg7_lut
	PORT MAP(iDIG => seg7_in3,
			oSEG => HEX_out1(6 DOWNTO 0));

	b2v_inst4 : seg7_lut
	PORT MAP(iDIG => seg7_in4,
			oSEG => HEX_out0(6 DOWNTO 0));

	b2v_inst5 : dig2dec
	PORT MAP(		 vol(REG_SIZE-1 downto 0) => sys_out,
			vol(15 downto REG_SIZE) => (others => '0'),
			seg0 => seg7_in4,
			seg1 => seg7_in3,
			seg2 => seg7_in2,
			seg3 => seg7_in1,
			seg4 => seg7_in0);

	HEX0 <= HEX_out0;
	HEX1 <= HEX_out1;
	HEX2 <= HEX_out2;
	HEX3 <= HEX_out3;
	HEX4 <= HEX_out4;
	HEX5(7) <= one;
	HEX5(6) <= one;
	HEX5(5) <= one;
	HEX5(4) <= one;
	HEX5(3) <= one;
	HEX5(2) <= one;
	HEX5(1) <= one;
	HEX5(0) <= one;

	zero <= '0';
	one <= '1';
	HEX_out0(7) <= '1';
	HEX_out1(7) <= '1';
	HEX_out2(7) <= '1';
	HEX_out3(7) <= '1';
	HEX_out4(7) <= '1';	

	LEDR <= SW;

	delay: process(MAX10_CLK1_50, RST)
	begin
		if RST = '1' then
			A_d <= (others => '0'); 
			B_d <= (others => '0');
			C_d <= (others => '0');
		elsif rising_edge(MAX10_CLK1_50) then
			if en = '0' or rw='1' then
				A_d <= A;
				B_d <= B;
				C_d <= C;
			else
				C_d <= ram_out; 
			end if;

		end if;
	end process delay;

	END bdf_type;