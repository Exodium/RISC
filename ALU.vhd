library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.formats.all;
use work.alu_opcodes.all;

entity ALU is 
	Port ( 
		clk : in std_logic;
		rst : in std_logic;
		A, B : in std_logic_vector (REG_SIZE-1 downto 0);

		alu_opcode : in std_logic_vector (ALU_OPCODE_SIZE-1 downto 0);
		C  : out std_logic_vector (REG_SIZE-1 downto 0);
		cond_out : out std_logic
	); 
end ALU;

architecture Behavioral of ALU is
	signal mul : std_logic_vector(2*REG_SIZE-1 downto 0);
begin
	process(clk,rst, A, B)
	begin
		if (rst = '1') then
			C <= (others => '0');
			cond_out <= '0';
			mul <= (others => '0');
		elsif rising_edge(clk) then
			case to_integer(unsigned(alu_opcode)) is
				when ALU_NOP => 
					C <= A;
					cond_out <= '0';
				when ALU_ADD => 
					C <= std_logic_vector(unsigned(A) + unsigned(B));
					if ( (unsigned(A)+unsigned(B)) srl REG_SIZE = 0 ) then cond_out <= '0';
																	 else cond_out <= '1'; end if;
				when ALU_SUB => 
					C <= std_logic_vector(unsigned(A) - unsigned(B));
					if ( (unsigned(A)+unsigned(B)) srl REG_SIZE = 0 ) then cond_out <= '0';
																	 else cond_out <= '1'; end if;
				when ALU_AND => 
					C <= A and B;
					if (to_integer(unsigned(A)) = 0)
						then cond_out <= '1';
						else cond_out <= '0'; end if;
				when ALU_OR  => 
					C <= A or B;
					if (to_integer(unsigned(A)) /= 0)
						then cond_out <= '1';
						else cond_out <= '0'; end if;
				when ALU_XOR => 
					C <= A xor B;
					cond_out <= '0';
				when ALU_NOR => 
					C <= A nor B;
					cond_out <= '0';
				when ALU_MUL => 
					C <= mul(REG_SIZE-1 downto 0);
					if (to_integer(unsigned(mul) srl REG_SIZE) = 0)	
						then cond_out <= '0';
						else cond_out <= '1'; 
						end if;
				when ALU_SLL => 
					C <= std_logic_vector( unsigned(A) sll to_integer(unsigned(B)));
					if ( A(to_integer(unsigned(A))) = '0') 
						then cond_out <= '0';
						else cond_out <= '1';
						end if;
				when ALU_SRL => 
					C <= std_logic_vector( unsigned(A) srl to_integer(unsigned(B)));
					cond_out <= '0';
				when others =>
					C <= (others => '0');
					cond_out <= '0';
			end case;
		end if;
		mul <= std_logic_vector(unsigned(A)* unsigned(B));
	end process;
end Behavioral;


