--- page de registres

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- package reg_constants
--	constant REG_ADR_SIZE : integer := 4;
--	constant REG_VAL_SIZE : integer := 8
--end package reg_constants;

use work.formats.all;

entity reg_table is
	generic (
		X0  : integer := 0; -- zero
		RA  : integer := 1; -- return address
		W0  : integer := 2; -- working registers 0-2
		W1  : integer := 3;
		W2  : integer := 4;
		S0  : integer := 5; -- permanent register 0-2
		S1  : integer := 6;
		S2  : integer := 7;
		A0  : integer := 8; -- argument registers 0-2
		A1  : integer := 9;
		A2  : integer := 10;
		R0  : integer := 11; -- return registers 0-2
		R1  : integer := 12;
		R2  : integer := 13;
		IM  : integer := 14; -- last immediate value in commands
		IO  : integer := 15 -- io pins
	);
	port(
		-- clock
		clk : in std_logic;
		
		-- reset
		rst : in std_logic;

		in_imm : in STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);

		in_adrA : in STD_LOGIC_VECTOR(ADR-1 downto 0);
		out_valA : out STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);

		in_adrB : in STD_LOGIC_VECTOR(ADR-1 downto 0);
		out_valB : out STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);


		in_adrC : in STD_LOGIC_VECTOR(ADR-1 downto 0);
		in_valC : in STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);


		in_sys_in : in STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);
		out_sys_out : out STD_LOGIC_VECTOR(REG_SIZE-1 downto 0));
end reg_table;

architecture reg_table_a of reg_table is
	type regs_type is array(0 to 12) of std_logic_vector(REG_SIZE-1 downto 0);
	signal regs : regs_type;

	-- la sortie est mémorisée (mais n'est pas accessible)
	signal last_out : STD_LOGIC_VECTOR(REG_SIZE-1 downto 0);

begin
	acces_reg_table:process(rst, clk)
	begin
		if rst='1' then
			for k in 0 to 12 loop
				regs(k) <= (others=>'0');
			end loop;
			last_out <= (others => '0');
			out_valA <= (others=>'0');
			out_valB <= (others=>'0');
			out_sys_out <=(others=>'0');
		else
			if rising_edge(clk) then
			-- gestion des sorties
				case ( to_integer(unsigned(in_adrA)) ) is -- sortie de A
					when X0 => out_valA <= (others=>'0') ; -- x0=0
					when RA => out_valA <= regs(0) ;
					when W0 => out_valA <= regs(1) ; 
					when W1 => out_valA <= regs(2) ; 
					when W2 => out_valA <= regs(3) ; 
					when S0 => out_valA <= regs(4) ; 
					when S1 => out_valA <= regs(5) ; 
					when S2 => out_valA <= regs(6) ; 
					when A0 => out_valA <= regs(7) ; 
					when A1 => out_valA <= regs(8) ; 
					when A2 => out_valA <= regs(9) ; 
					when R0 => out_valA <= regs(10);
					when R1 => out_valA <= regs(11); 
					when R2 => out_valA <= regs(12);
					when IM => out_valA <= in_imm; -- imm
					when IO => out_valA <= in_sys_in(REG_SIZE-1 downto 0); -- io0
					when others => out_valA <= (others=>'0') ;
				end case;
				case ( to_integer(unsigned(in_adrB)) ) is -- sortie de B
					when X0 => out_valB <= (others=>'0') ; -- x0=0
					when RA => out_valB <= regs(0) ;
					when W0 => out_valB <= regs(1) ;
					when W1 => out_valB <= regs(2) ;
					when W2 => out_valB <= regs(3) ;
					when S0 => out_valB <= regs(4) ;
					when S1 => out_valB <= regs(5) ;
					when S2 => out_valB <= regs(6) ;
					when A0 => out_valB <= regs(7) ;
					when A1 => out_valB <= regs(8) ;
					when A2 => out_valB <= regs(9) ;
					when R0 => out_valB <= regs(10);
					when R1 => out_valB <= regs(11);
					when R2 => out_valB <= regs(12);
					when IM => out_valB <= in_imm; -- imm
					when IO => out_valB <= in_sys_in(REG_SIZE-1 downto 0); -- io0
					when others => out_valB <= (others=>'0') ;
			end case;
				case ( to_integer(unsigned(in_adrC)) ) is -- sortie de B
					when X0 => null; -- x0 - ne rien faire
					when RA => regs(0)  <= in_valC;
					when W0 => regs(1)  <= in_valC;
					when W1 => regs(2)  <= in_valC;
					when W2 => regs(3)  <= in_valC;
					when S0 => regs(4)  <= in_valC;
					when S1 => regs(5)  <= in_valC;
					when S2 => regs(6)  <= in_valC;
					when A0 => regs(7)  <= in_valC;
					when A1 => regs(8)  <= in_valC;
					when A2 => regs(9)  <= in_valC;
					when R0 => regs(10) <= in_valC;
					when R1 => regs(11) <= in_valC;
					when R2 => regs(12) <= in_valC;
					when IM => null;-- imm - ne rien faire
					when IO => out_sys_out <= in_valC;-- io0
					when others => null;-- undef - ne rien faire
				end case;
			end if;
		end if;
	end process;
end architecture;
