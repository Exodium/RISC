
library ieee;
use ieee.std_logic_1164.all; 
use ieee.numeric_std.all;

use work.formats.all;
use work.commands.all;
use work.alu_opcodes.all;

entity command is
	Port ( 
		command_input : in STD_LOGIC_VECTOR(FULL_COMMAND_SIZE-1 downto 0);
		
		comp : in std_logic;
		
		
		-- arguments pour la file de registres
		cmd_addr_A : out STD_LOGIC_VECTOR(ADR-1 downto 0);
		cmd_addr_B : out STD_LOGIC_VECTOR(ADR-1 downto 0);
		
		-- execute
		alu_opcode : out STD_LOGIC_VECTOR(ALU_OPCODE_SIZE-1 downto 0);

		-- memory
		out_ram_enable : out std_logic;
		out_ram_rw : out std_logic;
		
		-- write back
		cmd_addr_C : out STD_LOGIC_VECTOR(ADR-1 downto 0);
		
		command_imm_value : out std_logic_vector(7 downto 0);

		jump : out std_logic;
		--out_jump : out std_logic;

		
		-- clock
		clk : in std_logic;
		-- reset
		rst : in std_logic
		);
end command;

architecture DecodeCommand of command is
	signal command_input_t1 : STD_LOGIC_VECTOR(FULL_COMMAND_SIZE-1 downto 0);
	signal command_input_t2 : STD_LOGIC_VECTOR(FULL_COMMAND_SIZE-1 downto 0);

	signal after_jmp, after_jmp2 : std_logic;
------------------------- Begin ----------------------------------------------------
	begin 
	
	

	-- partie synchrone
	process (clk, rst, command_input) begin

		-- instruction decode (async)
		cmd_addr_A <= command_input(19 downto 16); -- 4 bits du 2e octet
		cmd_addr_B <= command_input(15 downto 12); -- 4 bits du 2e octet
		command_imm_value <= command_input(7 downto 0); -- dernier octet

		if rst='1' then
			command_input_t1 <= (others => '0');
			command_input_t2 <= (others => '0');
			out_ram_enable <= '0';
			out_ram_rw <= '0';
			jump <= '0'; after_jmp <= '0'; after_jmp2 <= '0'; 

			cmd_addr_C <= (others=>'0');

			alu_opcode <= (others => '0');
			out_ram_enable <= '0';
			out_ram_rw <= '0';

		elsif rising_edge(clk) then
			-- s'il y a un saut il ne faut pas charger d'instruction supplémentaires
			if comp='1' and to_integer(unsigned(command_input_t1(COMMAND_START downto COMMAND_END))) = CMD_JMPc then -- todo and autre evaluation pour vérifier qu'on a bien reçu la consigne de jump
				command_input_t1 <= (others => '0');
				command_input_t2 <= (others => '0');

				jump <= '1';  after_jmp <= '1'; after_jmp2 <= '1';
				
				alu_opcode <= (others => '0');
				out_ram_enable <= '0';
				out_ram_rw <= '0';
				out_ram_enable <= '0';
				out_ram_rw <= '0';

				cmd_addr_C <= (others => '0');
			else
				-- prépare le prochain cycle
				jump <= '0';

				
				if (after_jmp = '0') then 
					-- write back (t2)
					if after_jmp2 = '0' then
						command_input_t2 <= command_input_t1;
						command_input_t1 <= command_input;
						cmd_addr_C <= command_input_t2(11 downto 8); -- 4 bits du 3e octet
					else 
						after_jmp2 <= '0';
						cmd_addr_C <= (others => '0');
					end if;

				else
					after_jmp <= '0';
					cmd_addr_C <= (others => '0');
				end if;

				-- execute (t0)
				case to_integer(unsigned(command_input(COMMAND_START downto COMMAND_END))) is
					when CMD_ADD   => alu_opcode <= std_logic_vector(to_unsigned(ALU_ADD, ALU_OPCODE_SIZE));
					when CMD_SUB   => alu_opcode <= std_logic_vector(to_unsigned(ALU_SUB, ALU_OPCODE_SIZE));
					when CMD_AND   => alu_opcode <= std_logic_vector(to_unsigned(ALU_AND, ALU_OPCODE_SIZE));
					when CMD_OR    => alu_opcode <= std_logic_vector(to_unsigned(ALU_OR , ALU_OPCODE_SIZE));
					when CMD_XOR   => alu_opcode <= std_logic_vector(to_unsigned(ALU_XOR, ALU_OPCODE_SIZE));
					when CMD_NOR   => alu_opcode <= std_logic_vector(to_unsigned(ALU_NOR, ALU_OPCODE_SIZE));
					when CMD_MUL   => alu_opcode <= std_logic_vector(to_unsigned(ALU_MUL, ALU_OPCODE_SIZE));
					when CMD_SLL   => alu_opcode <= std_logic_vector(to_unsigned(ALU_SLL, ALU_OPCODE_SIZE));
					when CMD_SRL   => alu_opcode <= std_logic_vector(to_unsigned(ALU_SRL, ALU_OPCODE_SIZE));
					when others    => alu_opcode <= std_logic_vector(to_unsigned(ALU_NOP, ALU_OPCODE_SIZE));
				end case;

				-- memory (t1)
				case to_integer(unsigned(command_input_t1(COMMAND_START downto COMMAND_END))) is
					when CMD_LOAD  => out_ram_enable <= '1'; out_ram_rw <= '0';
					when CMD_STORE => out_ram_enable <= '1'; out_ram_rw <= '1';
					when others    => out_ram_enable <= '0'; out_ram_rw <= '0';
				end case;
			end if;
		end if;
		
		
	end process;
end DecodeCommand;