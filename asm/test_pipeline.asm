mov im r0 7; // r0 <- 7
mov im r1 8; // r0 <- 7

cond_zero null;
jmpc a;

// les commandes suivantes sont sautées
mov im r0 0;
mov im r1 0;
mov im r2 1;
nop;
nop;

// l'éxecution reprend ici
label a;
mul r0 r1 r2; // r2 <- r0 * r1
nop;
nop;
nop;
mov r2 io; // sys_out <- r2
nop;
nop;
nop;
// si le test a passé
// r0 = 7; r1 = 56; r2 = 56; sys_out = 56
