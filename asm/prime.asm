// les mots comme start: sont des labels, ils sont à remplacer par l'adresse de l'instruction qui les suit
// ils permettent de se déplacer plus facilement dans le code
// je ferai un script python pour convertir ça automatiquement en binaire


// wait for the value to be different than 0
label start:

    mov im r1 2; // synthèse des deux commandes ci dessous 
    mov io r0;

    cond_zero r0; // (if r0 = 0 jump to start)

    jmpc start;

label loop:
    
    xor r0 r1 r6; // (r6 <- r0 ^ r1)  
    mov r0 r3; // r3 <- r0
    add r1 im r1 1; // (r1 <- r1 + 1)

    nop;
    nop;
    nop;
    // pourquoi un seul nop ?
    // il y a 3 instructions entre lecture et ecriture de r6 OK
    // pareil pour r3 (au minimum)
    // pareil pour r1

    cond_zero r6; // (if r6 = 0 jump to end), càd si on a testé tous les diviseurs
    jmpc r4_mod_r1; // on calcule le mod (et on boucle éventuellement)


label r4_mod_r1: // while r3-k*r1 > 0, k+=1

    // regarde le dernier bit
    and r3 im r2 128; // (r2 <- r3 & 128) avec l'ancien r3
    sub r3 r1 r3; // (r3 <- r3 - r1)
    
    nop;
    nop; // seulement deux nécessaires, (ya bien 3 instructions entre 2 usages)
    nop;


    
    cond_zero r3; // (if r3=0 jump to "end")
    jmpc end;

    cond_zero r2; // (if r2//128 == 0 jump to "r4 mod r1")
    jmpc r4_mod_r1; // (boucler si aucune des conditions precedentes n'est réalisée)
    cond_zero null; 
    jmpc loop;


label end:
    mov r1 io; // affiche le dernier diviseur
    cond_zero null; // (vrai tlt)
    jmpc start;

// peu importe que les instructions suivantes soient indeterminées, le pipeline sera vidé