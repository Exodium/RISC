
import sys
import spec


if len(sys.argv) > 1:
    in_filename = sys.argv[1]
    print("Assembler le fichier", in_filename)
else:
    print("Aucun argument n'a été passé.")
    exit()

out_filename = in_filename.split(".")[0] + ".bin"
out_file = open(out_filename, "wb")

out = bytearray()


# Ouvrir un fichier en mode lecture (par défaut)
with open(in_filename, 'r') as fichier:
    # Boucler sur chaque ligne du fichier
    lcount=0
    for ligne in fichier:
        lcount+=1
        ligne = ligne.lower().split(spec.line_comment)[0].replace("\t"," ").replace("\n","")
        spec.traite_ligne(ligne, out,lcount)
    vals = "";
    i = 0
    for p in spec.program:
        print(p[3],p[1])
        c = spec.completion(p[0],p[1],p[2],p[3])[1]
        b = ''.join(format(byte, '08b') for byte in c)
        vals += "Data_Rom({})".format(i) + ' <= "' + b + '"' + ";\n" 
        
        i+=1

    print(vals)

