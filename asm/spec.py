DEBUG=True


line_comment = "//"

operation_size = 8
register_size = 4
imm_size = 8

register_names = {
    "x0":0, "null":0,
    "r0":1,
    "r1":2,
    "r2":3,
    "r3":4,
    "r4":5,
    "r5":6,
    "r6":7,
    "r7":8,
    "r8":9,
    "r9":10,
    "r10":11,
    "r11":12,
    "r12":13,
    "im":14,
    "io":15,
}

commands = {
    "nop": 0, "mov": 0, "add": 0,
    "sub": 1, "neg": 1,
    "and": 2, "cond_zero": 2,
    "or":  3, "cond_nzero": 3,
    "xor": 4,
    "nor": 5,
    "mul": 6,
    "sll": 7,
    "srl": 8,
    "jmpc": 9,
    "load" : 10,
    "store" : 11,
}

syntaxes = {
    "nop":{ "":["x0","x0","x0",0] },
    "add":{ "arg arg arg": ["{}","{}","{}"] },
    "mov":{ "arg arg": ["{}", "x0", "{}"] },

    "sub":{ "arg arg arg": ["{}","{}","{}"] },
    "neg":{ "arg arg":["x0","{}","{}"] },
    
    "and":{ "arg arg arg":["{}","{}","{}"] }, "cond_zero": { "arg" : ["{}","null","null"] },

    "or":{ "arg arg arg":["{}","{}","{}"] },
    "xor":{ "arg arg arg":["{}","{}","{}"] },
    "not":{ "arg arg arg":["{}","{}","{}"] },
    "mul":{"arg arg arg":["{}","{}","{}"] },


    "jmpc" :{ "":["im","im","x0","x0"] }
}


program=[]

variables={}

def traite_ligne(line_arg : str, array, count=0):
    try:
        line = line_arg.lower().split(line_comment)[0].replace("\t"," ").replace("\n","")
        line = line_arg.replace(","," ")
        line = line.replace(";"," ")
        line = line.replace(":"," ")

        w = line.split(" ")
        line = ""
        for i in w:
            if i !="":
                line += i + " "
        line=line[:-1]
        if len(line)==0:
            return

        if DEBUG: print(count," ",line_arg)
        if DEBUG: print("\tRéduit : ", line)

        # line = "add r1 r2 r3"
        line=line.split(" ")

        if line[0] == "label":
            assert not (line[1] in variables.keys()) ,"Cannot edit a constant or use the same name as a label ({})".format(line[1])
            variables[line[1]] = len(program)
            print("Created label", line[1]," at destination", variables[line[1]])
            return
        elif line[0] == "const" and line[2] == "=":
            assert not (line[1] in variables.keys()), "Cannot edit a constant or use the same name as a label ({})".format(line[1])
            variables[line[1]] = eval(line[3])
            print("Created constant", line[1]," at destination", variables[line[1]])
            return

        commande = line[0]; args = line[1:] # cmd="add", args=["r0","r1","r2"]

        
        if DEBUG: print("\tTraité :\t", "cmd =", commande, "; args =", args)

        # complétion de la commande
        arg_formats = syntaxes[commande]
        
        # format utilisé dans la ligne
        format = ""
        use_imm = False
        for a in args:
            if a in register_names.keys():
                format += "arg "
            else:
                use_imm = True
        format = format[:-1]

        if DEBUG: print("\tFormat lu :\t", format)

        consigne_completion = arg_formats[format].copy()
        if DEBUG: print("\tComplete en :\t",consigne_completion)

        program.append((consigne_completion,args,use_imm,commande))

    except ValueError as ex: # exception générique
        print(ex)
        print("Erreur à la ligne {} :".format(count))
        print(count,"\t",line)
        exit()
    
def completion(consigne_completion, args, use_imm,commande):
            # completer les arguments
        arg_i = 0
        for i in range(3):
            if consigne_completion[i]=="{}":
                consigne_completion[i]=args[arg_i]
                arg_i += 1
        
        if arg_i < len(args) and use_imm: # s'il reste un argument c'est la valeur immédiate
            consigne_completion.append(args[arg_i])
        elif len(consigne_completion)==3:
            consigne_completion.append(0)

        if DEBUG: print("\tCompleté en :\t",consigne_completion)

        try:
            imm = int(consigne_completion[-1])
        except:
            print("\tReplace {} with {}.".format(consigne_completion[-1], variables[consigne_completion[-1]]))
            imm = int(variables[consigne_completion[-1]])

        codes = [ commands[commande] ] + [ register_names[consigne_completion[i]] for i in range(3) ] + [imm]

        if DEBUG: print("\tCodes :\t\t",codes)

        octets = [codes[0]*16+codes[1], 16*codes[2]+codes[3],codes[4]]
        mot3x8 = bytes(octets);
        return octets,mot3x8


#bytesArr = bytearray()